$(document).ready(function() {
  //************ ERROR BAR ************//
  let errorBar = document.getElementById('errorBar');
  let overlay = document.getElementById('overlay');
  let errorBarText = document.getElementById('errorBarText');

  function triggerErrorBar() {
    $(errorBar).fadeIn(200).css('display', 'flex');
    let isOverlayShown = overlay.classList.contains('show-overlay');
    overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }

  function dismissErrorBar() {
    $(errorBar).fadeOut(200);
    let isOverlayShown = overlay.classList.contains('show-overlay');
    overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }

  $('.js-dismiss-error-bar').on('click', function() {
    dismissErrorBar();
  });

  $('.js-trigger-error-bar').on('click', function() {
    triggerErrorBar();
  });
  //************ ERROR BAR ************//

  //************ SIZE VALIDATION ************//
  let myTubeSize,
      myTubeSizeNo,
      tubeDefaultLength;

  let tubeSize = document.getElementById('tubeSize');

  tubeSize.addEventListener("change", function() {
    myTubeSize = tubeSize.value;
  });

  function validateSize() {
    if (myTubeSize)
    {
      $('#sizeFieldset .js-go-forward').click();
    }
    else
    {
      errorBarText.innerHTML = "Please choose a size to advance."
      triggerErrorBar();
    }
  }

  function getMyTubeSizeNo() {
    if (myTubeSize === "M4DCT" || myTubeSize === "M4LPC") {
      myTubeSizeNo = 4;
    }
    else if (myTubeSize === "M6DFEN" || myTubeSize === "M6FEN" || myTubeSize === "MLGT6") {
      myTubeSizeNo = 6;
    }
    else if (myTubeSize === "M8DCFS" || myTubeSize === "M8CFS" || myTubeSize === "MLGT8") {
      myTubeSizeNo = 8;
    }
    else if (myTubeSize === "M10DCFN" || myTubeSize === "M10CFN" || myTubeSize === "MLGT") {
      myTubeSizeNo = 10;
    }
  }

  function getMyTubeLength() {
    if (myTubeSize == "M4DCT" || myTubeSize == "M4LPC") {
      tubeDefaultLength = 62;
    }
    else if (myTubeSize == "M6DFEN" || myTubeSize == "M6FEN") {
      tubeDefaultLength = 74;
    }
    else {
      tubeDefaultLength = 79;
    }
    displayMyTubeLength();
    updateCuffOptions();
  }

  function displayMyTubeLength() {
    document.getElementById('tubeDefaultLength').innerHTML = tubeDefaultLength;
  }

  // if tube is a Size 10, show the Oversized cuff option on Cuff screen
  function updateCuffOptions() {
    if (myTubeSize == "M10DCFN" || myTubeSize == "M10CFN" || myTubeSize == "MLGT10") {
      $('#tubeCuff .js-size-10-only').show();
    }
  }

  $('.js-validate-size').on('click', function() {
    getMyTubeLength();
    getMyTubeSizeNo();
    validateSize();
  });
  //************ SIZE VALIDATION ************//

  //************ LENGTH VALIDATION ************//
  let myTubeLength,
      newProximal = 0,
      newDistal = 0;

  let tubeLength = document.getElementById('tubeLength');
  let modifiedProximalLength = document.getElementById('modifiedProximalLength');
  let modifiedDistalLength = document.getElementById('modifiedDistalLength');

  modifiedProximalLength.addEventListener("keyup", function() {
    newProximal = parseInt(modifiedProximalLength.value);
    updateModifiedLength();
  });

  modifiedDistalLength.addEventListener("keyup", function() {
    newDistal = parseInt(modifiedDistalLength.value);
    updateModifiedLength();
  });

  tubeLength.addEventListener('change', function() {
    myTubeLength = tubeLength.value;
    // if 'modified' is selected, show modified inputs and total length
    if (myTubeLength === "modified") {
      $('#standardLengthText').hide();
      $('#modifiedLengthText').fadeIn(300);
    } else {
      // otherwise show the standard disclaimer
      $('#modifiedLengthText').hide();
      $('#standardLengthText').fadeIn(300);
    }
  });

  function updateModifiedLength() {
    totalModifiedLength = parseInt(newProximal + newDistal);
    document.getElementById('totalModifiedLength').value = totalModifiedLength;
    document.getElementById('showTotalModifiedLength').innerHTML = totalModifiedLength;
  }

  function validateLength() {
    if (myTubeLength == undefined || myTubeLength == "" || myTubeLength == null) {
      errorBarText.innerHTML = "Please choose a length to advance.";
      triggerErrorBar();
    }
    else if (myTubeLength === "modified") {
      if (newProximal == 0 || newDistal == 0) {
        errorBarText.innerHTML = "You must enter a proximal and a distal value to continue.";
        triggerErrorBar();
      }
      else if (totalModifiedLength < 40 || totalModifiedLength > 81) {
        errorBarText.innerHTML = "Your total length must be between 40 mm and 81 mm.";
        triggerErrorBar();
        return;
      }
      else {
        $('#lengthFieldset .js-go-forward').click();
      }
    }
    else {
      $('#lengthFieldset .js-go-forward').click();
    }
  }

  $('.js-validate-length').on('click', function() {
    validateLength();
  });
  //************ LENGTH VALIDATION ************//

  //************ CUFF VALIDATION ************//
  let myTubeCuff,
      currentCuff;

  let tubeCuff = document.getElementById('tubeCuff');

  tubeCuff.addEventListener('change', function() {
    myTubeCuff = tubeCuff.value;
    if (myTubeCuff == "uncuffed") {
      $('.js-rotate-flange').show();
    } else {
      // flange cannot be rotated unless cuff is 'uncuffed'
      $('.js-rotate-flange').hide();
    }
  });

  function validateCuff() {
    if (myTubeCuff)
    {
      $('#cuffFieldset .js-go-forward').click();
    }
    else
    {
      errorBarText.innerHTML = "Please choose a cuff to advance.";
      triggerErrorBar();
    }
  }

  $('.js-validate-cuff').on('click', function() {
    validateCuff();
  });
  //************ CUFF VALIDATION ************//

  //************ FLANGE VALIDATION ************//
  let myTubeFlange,
      modifiedFlangeRotation,
      flangeDirection;

  let tubeFlange = document.getElementById('tubeFlange');
  let rotateFlangeCheckbox = document.getElementById('rotateFlangeCheckbox');
  let flangeRotation = document.getElementById('modifiedFlangeRotation');
  let modifiedFlangeDirection = document.querySelector('input[name="modifiedFlangeDirection"]:checked').value;

  tubeFlange.addEventListener('change', function() {
    myTubeFlange = tubeFlange.value;
  });

  flangeRotation.addEventListener("input", function() {
    modifiedFlangeRotation = parseInt(flangeRotation.value);
  });

  function validateFlange() {
    if (myTubeFlange == undefined || myTubeFlange == "" || myTubeFlange == null) {
      errorBarText.innerHTML = "Please choose a flange to advance.";
      triggerErrorBar();
    }
    else if (myTubeCuff === "uncuffed") {
      if (rotateFlangeCheckbox.checked) {
        validateFlangeRotation();
      }
      else {
        $('#flangeFieldset .js-go-forward').click();
      }
    }
    else {
      $('#flangeFieldset .js-go-forward').click();
    }
  }

  function validateFlangeRotation() {
    if (modifiedFlangeRotation == 0 || modifiedFlangeRotation == undefined || modifiedFlangeRotation == null) {
      errorBarText.innerHTML = "You must enter a rotation value."
      triggerErrorBar();
    }
    else if (modifiedFlangeRotation > 40) {
      errorBarText.innerHTML = "Your rotation cannot exceed 40 degrees.";
      triggerErrorBar();
    }
    else {
      flangeDirection = modifiedFlangeDirection;
      $('#flangeFieldset .js-go-forward').click();
    }
  }

  $('.js-validate-flange').on('click', function() {
    validateFlange();
  });
  //************ FLANGE VALIDATION ************//

  //************ FENESTRATION VALIDATION ************//
  let tubeFenestration = document.getElementById("tubeFenestration");
  let fenestrationLength = document.getElementById('modifiedFenestrationLength');
  let fenestrationWidth = document.getElementById('modifiedFenestrationWidth');
  let fenestrationDistance = document.getElementById('modifiedFenestrationDistance');
  let myTubeFenestration,
      modifiedFenestrationLength,
      modifiedFenestrationWidth,
      modifiedFenestrationDistance;

  let modifiedFenestrationSize = false,
      modifiedFenestrationLocation = false;

  let hasLength, hasWidth, hasBothSizes, hasSizeInput, hasLocation, hasLocationInput;


  // listen for dropdown selection
  tubeFenestration.addEventListener("change", function() {
    myTubeFenestration = tubeFenestration.value;
    if (myTubeFenestration == "modified") {
      $('#standardFenestrationContent').hide();
      $('#modifiedFenestrationContent').fadeIn(300);
    } else {
      $('#modifiedFenestrationContent').hide();
      $('#standardFenestrationContent').fadeIn(300);
    }
  });

  fenestrationWidth.addEventListener("input", function() {
    modifiedFenestrationWidth = fenestrationWidth.value;
  });

  fenestrationLength.addEventListener("input", function() {
    modifiedFenestrationLength = fenestrationLength.value;
  });

  fenestrationDistance.addEventListener("input", function() {
    modifiedFenestrationDistance = fenestrationDistance.value;
  });

  function checkSize() {
    if (modifiedFenestrationLength) {
      hasLength = true;
    } else {
      hasLength = false;
    }

    if (modifiedFenestrationWidth) {
      hasWidth = true;
    } else {
      hasWidth = false;
    }

    if (hasLength === true && hasWidth === true) {
      hasBothSizes = true;
      modifiedFenestrationLength = parseInt(modifiedFenestrationLength);
      modifiedFenestrationWidth = parseInt(modifiedFenestrationWidth);
    } else {
      hasBothSizes = false;
    }

    if (hasLength === true && hasWidth === false) {
      hasBothSizes = false;
    } else if (hasWidth === true && hasLength === false) {
      hasBothSizes = false;
    }

    if ((modifiedFenestrationLength == "" || modifiedFenestrationLength == undefined || modifiedFenestrationLength == null) && (modifiedFenestrationWidth == "" || modifiedFenestrationWidth == undefined || modifiedFenestrationWidth == null)) {
      hasSizeInput = false;
    } else {
      hasSizeInput = true;
    }
  }

  function checkLocation() {
    if (modifiedFenestrationDistance) {
      hasLocation = true;
      modifiedFenestrationDistance = parseInt(modifiedFenestrationDistance);
    } else {
      hasLocation = false;
      modifiedFenestrationDistance = 0;
    }

    if (modifiedFenestrationDistance == "" || modifiedFenestrationDistance == undefined || modifiedFenestrationDistance == null || modifiedFenestrationDistance == 0) {
      hasLocationInput = false;
    } else {
      hasLocationInput = true;
    }
  }

  function validateFenestration() {
    if (myTubeFenestration && myTubeFenestration == "modified") {
      // check for size and location
      checkSize();
      checkLocation();
      if (hasLocation === true || hasBothSizes === true) {
        $('#fenestrationFieldset .js-go-forward').click();
      } else {
        errorBarText.innerHTML = "You must enter values for size or location to advance with a modified fenestration."
        triggerErrorBar();
      }
    } else if (myTubeFenestration) {
      $('#fenestrationFieldset .js-go-forward').click();
    } else {
      errorBarText.innerHTML = "Please choose your fenestration to advance."
      triggerErrorBar();
    }
  }

  $('.js-save-fenestration-size').on('click', function() {
    checkSize();
    if (hasBothSizes === true && hasSizeInput === true) {
      var _this = $(this);
      closeAccordion(_this);
      modifiedFenestrationLength = parseInt(modifiedFenestrationLength);
      modifiedFenestrationWidth = parseInt(modifiedFenestrationWidth);
    }
    else if (hasBothSizes === false) {
      errorBarText.innerHTML = "Please enter a length and width to modify your fenestration size."
      triggerErrorBar();
    }
  });

  $('.js-save-fenestration-distance').on('click', function() {
    checkLocation();
    if (hasLocation && hasLocationInput) {
      var _this = $(this);
      closeAccordion(_this);
      modifiedFenestrationDistance = parseInt(modifiedFenestrationDistance);
    }
    else {
      errorBarText.innerHTML = "Please enter your distance to advance."
      triggerErrorBar();
    }
  });

  function closeAccordion(_this) {
    var thisContent = _this.parent('.js-accordion-content');
    var thisIcon = _this.parent().siblings().children('.js-accordion-icon');
    var thisTrigger = _this.parent().siblings('.js-accordion-trigger');
    // Close the answer item - it's already open
    thisContent.removeClass('is-active').slideUp(300);
    // change the icon to its default state
    thisIcon.removeClass('mi-btb-minus').addClass('mi-btb-plus').removeClass('is-active');
  }

  $('.js-validate-fenestration').on('click', function() {
    validateFenestration();
  });
  //************ FENESTRATION VALIDATION ************//

  //************ TUBE QUANTITY ************//
  let tubeQuantity = document.getElementById('tubeQuantity');
  let myTubeQuantity = tubeQuantity.value;
  $('.js-show-tube-quantity').html(myTubeQuantity);

  tubeQuantity.addEventListener("change", function() {
    myTubeQuantity = tubeQuantity.value;
    $('.js-show-tube-quantity').html(myTubeQuantity);
  });
  //************ TUBE QUANTITY ************//

  //************ FINALIZE ORDER REQUEST  ************//
  $('#finalizeRequest').on('click', function() {
    finalizeValues();
    displayValues();
  });

  function finalizeValues() {
    myTubeSize = myTubeSize;
    myTubeSizeNo = myTubeSizeNo;
    myTubeLength = capitalizeFirstLetter(myTubeLength);
    myTubeCuff = capitalizeFirstLetter(myTubeCuff);
    myTubeFlange = capitalizeFirstLetter(myTubeFlange);
    myTubeFenestration = capitalizeFirstLetter(myTubeFenestration);
  }

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function displayValues() {
    showMyTubeSize();
    showMyTubeSizeNo();
    showMyTubeLength();
    showMyTubeCuff();
    showMyTubeFlange();
    showMyTubeFenestration();
  }

  function showMyTubeSize() {
    $('.js-show-tube-size').html(myTubeSize);
  }

  function showMyTubeSizeNo() {
    $('.js-show-tube-size-no').html(myTubeSizeNo);
  }

  function showMyTubeLength() {
    $('.js-show-tube-length').html(myTubeLength);

    if (myTubeLength === "Standard") {
      $('.js-if-modified-length').hide();
      $('.js-if-standard-length').show();
      $('.js-show-tube-default-length').html(tubeDefaultLength);
    } else {
      $('.js-if-standard-length').hide();
      $('.js-if-modified-length').show();
      $('.js-show-modified-proximal-length').html(newProximal);
      $('.js-show-modified-distal-length').html(newDistal);
      $('.js-show-total-modified-length').html(totalModifiedLength);
    }
  }

  function showMyTubeCuff() {
    $('.js-show-tube-cuff').html(myTubeCuff);
  }

  function showMyTubeFlange() {
    $('.js-show-tube-flange').html(myTubeFlange);

    if (modifiedFlangeRotation) {
      $('.js-if-rotated-flange').show();
      $('.js-show-modified-flange-rotation').html(modifiedFlangeRotation);
      $('.js-show-modified-flange-direction').html(flangeDirection);
    } else {
      $('.js-if-rotated-flange').hide();
    }
  }

  function showMyTubeFenestration() {
    $('.js-show-tube-fenestration').html(myTubeFenestration);

    if (hasLocation === true) {
      $('.js-if-modified-fenestration-location').show();
      $('.js-show-modified-fenestration-distance').html(modifiedFenestrationDistance);
    } else {
      $('.js-if-modified-fenestration-location').hide();
    }

    if (hasBothSizes === true) {
      $('.js-if-modified-fenestration-size').show();
      $('.js-show-modified-fenestration-length').html(modifiedFenestrationLength);
      $('.js-show-modified-fenestration-width').html(modifiedFenestrationWidth);
    } else {
      $('.js-if-modified-fenestration-size').hide();
    }
  }
  //************ FINALIZE ORDER REQUEST ************//
});
