$(document).ready(function() {

  //************ VALIDATE CUSTOMER INFO ************//
  $('.js-validate-customer-info').on('click', function() {
    // validate form input
    ORDER.validateForm();
  });

  ;ORDER = {

    validateForm: function() {
      // vars
      let purchaseOrderNum   = $('#purchaseOrderNum');
      let shipToAccountNum   = $('#shipToAccountNum');
      let country            = $('#country');
      let facility           = $('input[id="facility"]');
      let address            = $('input[id="address"]');
      let city               = $('input[id="city"]');
      let state              = $('input[id="state"]');
      let zip                = $('input[id="zip"]');
      let email              = $('input[id="email"]');
      let phone              = $('#phone');
      let instructions       = $('#instructions');

      let billingCountry     = $('#billingCountry');
      let billToAccountNum   = $('#billToAccountNum');
      let billingFacility    = $('input[id="billingFacility"]');
      let billingAddress     = $('input[id="billingAddress"]');
      let billingCity        = $('input[id="billingCity"]');
      let billingState       = $('input[id="billingState"]');
      let billingZip         = $('input[id="billingZip"]');
      let billingEmail       = $('input[id="billingEmail"]');
      let billingPhone       = $('input[id="billingPhone"]');

      let fields = [];
      let approvedInput = [];
      let isValid,
          diffBillingInfo,
          zipRegex,
          billZipRegex;

      let hasDifferentBillingInfo = $('input[name="billingInformation"]:checked').val();

      // push billing fields into array if different billing info
      if (hasDifferentBillingInfo === "differentBilling") {
        diffBillingInfo = true;
        fields.push(purchaseOrderNum, shipToAccountNum, country, facility, address, city, state, zip, email, billingCountry, billToAccountNum, billingFacility, billingAddress, billingCity, billingState, billingZip, billingEmail);
      } else {
        // otherwise just push basic fields
        fields.push(purchaseOrderNum, shipToAccountNum, country, facility, address, city, state, zip, email);
      }

      // test for any input 2 chars or more
      let generalRegex = /^(([a-zA-Z ]|[0-9 ])|([-]|[_]|[.])){1,40}$/;
      let stringRegex = /^([a-zA-Z -]){1,40}$/;
      let numRegex = /^[0-9]{1,5}$/;
      // international email test
      let emailRegex = /^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$/;
      // united states zip code
      let usaZipRegex = /^\d{5}([ \-]\d{4})?$/;
      // canadian zip code
      let canadaZipRegex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d$/;
      // israeli zip code
      let israelZipRegex = /^\\b\\d{5}(\\d{2})?$/;

      let myCountry = $('#country').val();
      let myBillingCountry = $('#billingCountry').val();

      let myState = $('#state').val();
      let myBillingState = $('#billingState').val();

      // based on country selection, add state/province to required fields
      // add appropriate zip regex
      if (myCountry === "USA") {
        zipRegex = usaZipRegex;
      } else if (myCountry === "Canada") {
        zipRegex = canadaZipRegex;
      } else if (myCountry === "Israel") {
        zipRegex = israelZipRegex;
        myState = "na";
      } else {
        zipRegex = /^\d{5}([ \-]\d{4})?$/;
      }

      // do the same for billing fields if applicable
      if (myBillingCountry === "USA") {
        billZipRegex = usaZipRegex;
      } else if (myBillingCountry === "Canada") {
        billZipRegex = canadaZipRegex;
      } else if (myBillingCountry === "Israel") {
        billZipRegex = israelZipRegex;
        myBillingState = "na";
      } else {
        billZipRegex = /^\d{5}([ \-]\d{4})?$/;
      }

      // get customer inputs
      let  myPurchaseOrderNum = $(purchaseOrderNum).val();
      let  myShipToAccountNum = $(shipToAccountNum).val();
      let  myFacility = $(facility).val();
      let  myAddress = $(address).val();
      let  myCity = $(city).val();
      let  myZip = $(zip).val();
      let  myEmail = $(email).val();
      let  myPhone = $(phone).val();
      let  myInstructions = $(instructions).val();
      // get billing input if applicable
      let myBillToAccountNum = $(billToAccountNum).val();
      let myBillingFacility = $(billingFacility).val();
      let myBillingAddress = $(billingAddress).val();
      let myBillingCity = $(billingCity).val();
      let myBillingZip = $(billingZip).val();
      let myBillingEmail = $(billingEmail).val();
      let myBillingPhone = $(billingPhone).val();

      // display required fields
      $('.js-show-purchase-order-num').html(myPurchaseOrderNum);
      $('.js-show-ship-to-account-num').html(myShipToAccountNum);
      $('.js-show-facility').html(myFacility);
      $('.js-show-address').html(myAddress);
      $('.js-show-city').html(myCity);
      $('.js-show-state').html(myState);
      $('.js-show-zip').html(myZip);
      $('.js-show-country').html(myCountry);
      $('.js-show-email').html(myEmail);

      // optional fields - display input if vals have been entered
      if (myPhone) {
        $('.js-show-phone').html(myPhone);
      } else {
        $('.js-show-phone').html("Not provided");
      }

      if (myInstructions) {
        $('.js-show-special-instructions').html(myInstructions);
      } else {
        $('.js-show-special-instructions').html("Not provided");
      }

      // do the same for billing fields if applicable
      if (diffBillingInfo === true) {
        // only show the billing info container if they've selected different billing info
        $('#billingInfo').show();

        // required fields
        $('.js-show-bill-to-account-num').html(myBillToAccountNum);
        $('.js-show-billing-facility').html(myBillingFacility);
        $('.js-show-billing-address').html(myBillingAddress);
        $('.js-show-billing-city').html(myBillingCity);
        $('.js-show-billing-state').html(myBillingState);
        $('.js-show-billing-zip').html(myBillingZip);
        $('.js-show-billing-country').html(myBillingCountry);
        $('.js-show-billing-email').html(myBillingEmail);

        // optional fields
        if (myBillingPhone) {
          $('.js-show-billing-phone').html(myBillingPhone);
        } else {
          $('.js-show-billing-phone').html("Not provided");
        }

      } else {
        // if the billing info is the same as shipping, don't show the billing information at all
        $('#billingInfo').hide();
      }

      function validateInput(element, index, array) {
        let $element = element;
        let value = $element.val();
        let myLength = value.length;
        let myTest;

        // add different tests based on the field requirements
        if ($element.hasClass("js-email")) {
          myTest = emailRegex.test(value);
        } else if ($element.hasClass("js-zip")) {
          myTest = zipRegex.test(value);
        } else if ($element.hasClass("js-billing-zip")) {
          myTest = billZipRegex.test(value);
        } else if ($element.hasClass('is-num')) {
          myTest = numRegex.test(value);
        }
          else {
          myTest = generalRegex.test(value);
        }

        if (myTest == false || myLength < 1) {
          $element.addClass('has-focus');
          // show the error message if input doesn't pass
          $element.next().show();
          isValid = false;
        } else if (myTest === true && myLength >= 1) {
          $element.removeClass('has-focus');
          // hide the error message if input passes the test
          $element.next().hide();
          // isValid = true;
          approvedInput.push(value);
        }
      }

      fields.forEach(validateInput);

      if (approvedInput.length === fields.length) {
        isValid = true;
        $('#customerInfo .js-go-forward').click();
        $('.js-nav-items').find('.js-nav-item[data-attr="orderSummary"]').addClass('is-active');
        $('.js-nav-items').find('.js-nav-item[data-attr="customerInfo"]').addClass('is-validated');
      } else {
        isValid = false;
      }
    }
  }

  //************ FINAL SUBMIT ************//
  // extra submit button on page - make it click the real submit button
  var clickSubmit = document.getElementById('clickSubmitButton');
  var submitButton = document.getElementById('submitButton');

  clickSubmit.addEventListener('click', function() {
    submitButton.click();
  });
  //************ FINAL SUBMIT ************//
});
