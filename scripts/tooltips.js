$(document).ready(function() {

  var tooltip = $('.js-tooltip');
  var tooltipToggler = $('.js-tooltip-toggler');

  $(tooltipToggler).on('click', function() {
    console.log('click');
    if ($(this).hasClass('mi-mitg-shiley-help')) {
      $(this).removeClass('mi-mitg-shiley-help').addClass('mi-mitg-shiley-close');
    }
    else {
      $(this).removeClass('mi-mitg-shiley-close').addClass('mi-mitg-shiley-help');
    }
    $(this).siblings().children('.js-tooltip').toggleClass('is-shown');
  });

});
