$(document).ready(function() {

  //************ ERROR BAR ************//
  let errorBar = document.getElementById('errorBar');
  let overlay = document.getElementById('overlay');
  let errorBarText = document.getElementById('errorBarText');

  function triggerErrorBar() {
    $(errorBar).fadeIn(200).css('display', 'flex');
    let isOverlayShown = overlay.classList.contains('show-overlay');
    overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }

  function dismissErrorBar() {
    $(errorBar).fadeOut(200);
    let isOverlayShown = overlay.classList.contains('show-overlay');
    overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }

  $('.js-dismiss-error-bar').on('click', function() {
    dismissErrorBar();
  });

  $('.js-trigger-error-bar').on('click', function() {
    triggerErrorBar();
  });
  //************ ERROR BAR ************//


  //************ PRODUCT CODE & SIZE VALIDATION ************//
  // neopeds product code/ size dropdowns
  let myProductCode,
      myTubeSize,
      myProximal,
      myDistal,
      myTotalLength,
      newProximal = 0,
      newDistal = 0,
      totalModifiedLength,
      tubeCuff,
      myTubeCuff,
      currentCuff;

  let productCode = document.getElementById('productCode');
  let sizeDropdowns = $('#sizeDropdowns');
  let sizeLabel = $('.js-size-label');

  if (productCode) {
    $(sizeDropdowns).hide();
    $(sizeLabel).hide();
    $('#sizeDropdowns').hide();
    productCode.addEventListener("change", function() {
      myProductCode = document.getElementById('productCode').value;
      // get dropdown selection
      let thisCode = productCode.value;
      $(sizeLabel).fadeIn(300);
      // get container with size dropdowns
      let sizeDropdowns = $('#sizeDropdowns');
      $(sizeDropdowns).fadeIn(300);
      // hide dropdowns that don't match
      $(sizeDropdowns).find('.js-size-selector:not([data-code="' + thisCode + '"])').hide();
      // show dropdown that matches
      $(sizeDropdowns).find('.js-size-selector[data-code="' + thisCode + '"]').fadeIn(300);
      // show and hide correct flanges based on size selected
      // let myProductCode = $("select[name=productCode]").val();
      if (myProductCode === "mnt" || myProductCode === "mpt" || myProductCode === "mneo" || myProductCode === "mped") {
        $('#tubeFlange .js-pediatric-flange').hide();
        $('#pediatricFlangeImage').hide();
        $('#tubeFlange .js-neonatal-flange').show();
        $('#neonatalFlangeImage').show();
      } else if (myProductCode === "mpdc" || myProductCode === "mplc" || myProductCode === "mpdl") {
        $('#tubeFlange .js-neonatal-flange').hide();
        $('#neonatalFlangeImage').hide();
        $('#tubeFlange .js-pediatric-flange').show();
        $('#pediatricFlangeImage').show();
      }
    });
  }

  function validateSize() {
    if (myProductCode == undefined) {
      errorBarText.innerHTML = "You must choose a product code in order to choose a size";
      triggerErrorBar();
    } else if (myProductCode !== undefined && myTubeSize == undefined) {
      errorBarText.innerHTML = "Please choose a size to advance"
      triggerErrorBar();
    } else {
      $('#sizeFieldset .js-go-forward').click();
    }
  }

  function updateCuff() {
    if (myProductCode === "mpdc" || myProductCode === "mplc") {
      tubeCuff = document.getElementById('tubeCuff').value = "standard";
      myTubeCuff = "standard";
      document.getElementById('currentCuff').innerHTML = "Standard";
      $('#standardCuffText').show();
      $('#uncuffedText').hide();
      // flange cannot be rotated unless cuff is 'uncuffed'
      $('.js-rotate-flange').hide();
      // on fenestration, do not show an option to modify
      $('#tubeFenestration .js-modified-fenestration-option').hide();
    } else {
      tubeCuff = document.getElementById('tubeCuff').value = "uncuffed";
      myTubeCuff = "uncuffed";
      document.getElementById('currentCuff').innerHTML = "Uncuffed";
      $('#standardCuffText').hide();
      $('#uncuffedText').show();
      // on fenestration, do show the option to modify
      $('#tubeFenestration .js-modified-fenestration-option').show();
    }
  }

  $('.js-validate-size').on('click', function() {
    validateSize();
    updateCuff();
  });

  let mpdcValues = document.getElementById('mpdcValues');
  let mplcValues = document.getElementById('mplcValues');
  let mntValues = document.getElementById('mntValues');
  let mptValues = document.getElementById('mptValues');
  let mneoValues = document.getElementById('mneoValues');
  let mpedValues = document.getElementById('mpedValues');
  let mpdlValues = document.getElementById('mpdlValues');
  let modifiedProximalLength = document.getElementById('modifiedProximalLength');
  let modifiedDistalLength = document.getElementById('modifiedDistalLength');

  modifiedProximalLength.addEventListener("keyup", function() {
    newProximal = parseInt(modifiedProximalLength.value);
    updateModifiedLength();
  });

  modifiedDistalLength.addEventListener("keyup", function() {
    newDistal = parseInt(modifiedDistalLength.value);
    updateModifiedLength();
  });

  function updateModifiedLength() {
    totalModifiedLength = parseInt(newProximal + newDistal);
    document.getElementById('totalModifiedLength').value = totalModifiedLength;
    document.getElementById('showTotalModifiedLength').innerHTML = totalModifiedLength;
  }

  mpdcValues.addEventListener("change", function() {
    myTubeSize = mpdcValues.value;
    myProximal = 0;
    if (myTubeSize === "4.0") {
      myDistal = 50;
    }
    else if (myTubeSize === "4.5") {
      myDistal = 52;
    }
    else if (myTubeSize === "5.0") {
      myDistal = 54;
    }
    else if (myTubeSize === "5.5") {
      myDistal = 56;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mplcValues.addEventListener("change", function() {
    myTubeSize = mplcValues.value;
    myProximal = 0;
    if (myTubeSize === "5.0") {
      myDistal = 41;
    }
    else if (myTubeSize === "5.5") {
      myDistal = 42;
    }
    else if (myTubeSize === "6.0") {
      myDistal = 44;
    }
    else if (myTubeSize === "6.5") {
      myDistal = 46;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mntValues.addEventListener("change", function() {
    myTubeSize = mntValues.value;
    myProximal = 0;
    if (myTubeSize === "2.5") {
      myDistal = 30;
    }
    else if (myTubeSize === "3.0") {
      myDistal = 32;
    }
    else if (myTubeSize === "3.5") {
      myDistal = 34;
    }
    else if (myTubeSize === "4.0") {
      myDistal = 36;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mptValues.addEventListener("change", function() {
    myTubeSize = mptValues.value;
    myProximal = 0;
    if (myTubeSize === "2.5") {
      myDistal = 39;
    }
    else if (myTubeSize === "3.0") {
      myDistal = 40;
    }
    else if (myTubeSize === "3.5") {
      myDistal = 41;
    }
    else if (myTubeSize === "4.0") {
      myDistal = 42;
    }
    else if (myTubeSize === "4.5") {
      myDistal = 44;
    }
    else if (myTubeSize === "5.0" || myTubeSize === "5.5" || myTubeSize === "6.0") {
      myDistal = 46;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mneoValues.addEventListener("change", function() {
    myTubeSize = mneoValues.value;
    myProximal = 0;
    if (myTubeSize === "3.0") {
      myDistal = 30;
    }
    else if (myTubeSize === "3.5") {
      myDistal = 32;
    }
    else if (myTubeSize === "4.0") {
      myDistal = 34;
    }
    else if (myTubeSize === "4.5") {
      myDistal = 36;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mpedValues.addEventListener("change", function() {
    myTubeSize = mpedValues.value;
    myProximal = 0;
    if (myTubeSize === "3.0") {
      myDistal = 39;
    }
    else if (myTubeSize === "3.5") {
      myDistal = 40;
    }
    else if (myTubeSize === "4.0") {
      myDistal = 41;
    }
    else if (myTubeSize === "4.5") {
      myDistal = 42;
    }
    else if (myTubeSize === "5.0") {
      myDistal = 44;
    }
    else if (myTubeSize === "5.5") {
      myDistal = 46;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  mpdlValues.addEventListener("change", function() {
    myTubeSize = mpdlValues.value;
    myProximal = 0;
    if (myTubeSize === "5.0") {
      myDistal = 50;
    }
    else if (myTubeSize === "5.5") {
      myDistal = 52;
    }
    else if (myTubeSize === "6.0") {
      myDistal = 54;
    }
    else if (myTubeSize === "6.5") {
      myDistal = 56;
    }
    myTotalLength = parseInt(myDistal + myProximal);
    updateValues();
  });

  function updateValues() {
    document.getElementById('currentSize').innerHTML = myTubeSize;
    document.getElementById('yourCurrentSize').innerHTML = myTubeSize;
    document.getElementById('currentDistal').innerHTML = myDistal;
    document.getElementById('currentTotalLength').innerHTML = myTotalLength;
  }
  //************ PRODUCT CODE & SIZE VALIDATION ************//

  //************ LENGTH VALIDATION ************//
  let myTubeLength;
  let tubeLength = document.getElementById('tubeLength');
  // listen for dropdown selection
  tubeLength.addEventListener("change", function() {
    myTubeLength = tubeLength.value;
    // if 'modified' is selected, show modified inputs and total length
    if (myTubeLength == "modified") {
      $('#standardLengthText').hide();
      $('#modifiedLengthText').fadeIn(300);
    } else {
      // otherwise show the standard disclaimer
      $('#modifiedLengthText').hide();
      $('#standardLengthText').fadeIn(300);
    }
  });

  function validateLength() {
    if (myTubeLength == undefined) {
      errorBarText.innerHTML = "Please choose a length to advance.";
      triggerErrorBar();
    } else if (myTubeLength == "modified") {
      if (newProximal == 0 || newDistal == 0) {
        errorBarText.innerHTML = "You must enter a proximal and a distal value to continue.";
        triggerErrorBar();
      } else {
        if (myTubeSize >= "2.5" && myTubeSize <= "3.5") {
          if (totalModifiedLength < 20 || totalModifiedLength > 120) {
            errorBarText.innerHTML = "Your total length must be between 20 mm and 120 mm.";
            triggerErrorBar();
            return;
          } else {
            $('#lengthFieldset .js-go-forward').click();
          }
        } else {
          if (totalModifiedLength < 30 || totalModifiedLength > 170) {
            errorBarText.innerHTML = "Your total length must be between 30 mm and 170 mm.";
            triggerErrorBar();
            return;
          } else {
            $('#lengthFieldset .js-go-forward').click();
          }
        }
      }
    } else {
      $('#lengthFieldset .js-go-forward').click();
    }
  }

  $('.js-validate-length').on('click', function() {
    validateLength();
  });
  //************ LENGTH VALIDATION ************//

  //************ CUFF VALIDATION ************//
  $('.js-validate-cuff').on('click', function() {
    $('#cuffFieldset .js-go-forward').click();
  });
  //************ CUFF VALIDATION ************//


  //************ FLANGE VALIDATION ************//
  let myTubeFlange,
      modifiedFlangeRotation,
      flangeDirection;

  let tubeFlange = document.getElementById('tubeFlange');
  let rotateFlangeCheckbox = document.getElementById('rotateFlangeCheckbox');
  let flangeRotation = document.getElementById('modifiedFlangeRotation');
  let modifiedFlangeDirection = document.querySelector('input[name="modifiedFlangeDirection"]:checked').value;

  tubeFlange.addEventListener('change', function() {
    myTubeFlange = tubeFlange.value;
  });

  flangeRotation.addEventListener("input", function() {
    modifiedFlangeRotation = parseInt(flangeRotation.value);
  });

  function validateFlange() {
    if (myTubeFlange == undefined) {
      errorBarText.innerHTML = "Please choose a flange to advance";
      triggerErrorBar();
    }
    else if (myTubeCuff == "uncuffed") {
      if (rotateFlangeCheckbox.checked) {
        validateFlangeRotation();
      }
      else {
        $('#flangeFieldset .js-go-forward').click();
      }
    }
    else {
      $('#flangeFieldset .js-go-forward').click();
    }
  }

  function validateFlangeRotation() {
    if (modifiedFlangeRotation == 0 || modifiedFlangeRotation == undefined) {
      errorBarText.innerHTML = "You must enter a rotation value."
      triggerErrorBar();
    }
    else if (modifiedFlangeRotation > 40) {
      errorBarText.innerHTML = "Your rotation cannot exceed 40 degrees.";
      triggerErrorBar();
    }
    else {
      flangeDirection = modifiedFlangeDirection;
      $('#flangeFieldset .js-go-forward').click();
    }
  }

  $('.js-validate-flange').on('click', function() {
    validateFlange();
  });
  //************ FLANGE VALIDATION ************//


  //************ CURVE VALIDATION ************//
  let tubeCurve = document.getElementById('tubeCurve');
  let modifiedTubeCurve = document.getElementById('modifiedTubeCurve');
  let myTubeCurve,
      modifiedCurveAngle;

  tubeCurve.addEventListener("change", function() {
    myTubeCurve = tubeCurve.value;
    // if 'modified' is selected, show modified inputs and total curve
    if (myTubeCurve == "modified") {
      $('#modifiedCurveText').slideDown(300);
    } else {
      // otherwise show the standard disclaimer
      $('#modifiedCurveText').slideUp(300);
    }
  });

  modifiedTubeCurve.addEventListener("input", function() {
    modifiedCurveAngle = modifiedTubeCurve.value;
  });

  function validateCurve() {
    if (myTubeCurve == undefined) {
      errorBarText.innerHTML = "Please select an option to advance."
      triggerErrorBar();
    } else if (myTubeCurve == "modified") {
      if (modifiedCurveAngle == undefined) {
        errorBarText.innerHTML = "Please enter a curve angle."
        triggerErrorBar();
      }
      else if (modifiedCurveAngle < 80 || modifiedCurveAngle > 160) {
        errorBarText.innerHTML = "Curve angle must be between 80 and 160 degrees."
        triggerErrorBar();
      }
      else {
        $('#curveFieldset .js-go-forward').click();
      }
    } else {
      $('#curveFieldset .js-go-forward').click();
    }
  }

  $('.js-validate-curve').on('click', function() {
    validateCurve();
  });
  //************ CURVE VALIDATION ************//

  //************ FENESTRATION VALIDATION ************//
  let tubeFenestration = document.getElementById("tubeFenestration");
  let fenestrationLength = document.getElementById('modifiedFenestrationLength');
  let fenestrationWidth = document.getElementById('modifiedFenestrationWidth');
  let fenestrationDistance = document.getElementById('modifiedFenestrationDistance');
  let myTubeFenestration,
      modifiedFenestrationLength,
      modifiedFenestrationWidth,
      modifiedFenestrationDistance;

  let modifiedFenestrationSize = false,
      modifiedFenestrationLocation = false;

  let hasLength, hasWidth, hasBothSizes, hasSizeInput, hasLocation, hasLocationInput;

  // listen for dropdown selection
  tubeFenestration.addEventListener("change", function() {
    myTubeFenestration = tubeFenestration.value;
    if (myTubeFenestration == "modified") {
      $('#standardFenestrationContent').hide();
      $('#modifiedFenestrationContent').fadeIn(300);
    } else {
      $('#modifiedFenestrationContent').hide();
      $('#standardFenestrationContent').fadeIn(300);
    }
  });

  fenestrationWidth.addEventListener("input", function() {
    modifiedFenestrationWidth = fenestrationWidth.value;
  });

  fenestrationLength.addEventListener("input", function() {
    modifiedFenestrationLength = fenestrationLength.value;
  });

  fenestrationDistance.addEventListener("input", function() {
    modifiedFenestrationDistance = fenestrationDistance.value;
  });

  function checkSize() {
    if (modifiedFenestrationLength) {
      hasLength = true;
    } else {
      hasLength = false;
    }

    if (modifiedFenestrationWidth) {
      hasWidth = true;
    } else {
      hasWidth = false;
    }

    if (hasLength === true && hasWidth === true) {
      hasBothSizes = true;
      modifiedFenestrationLength = parseInt(modifiedFenestrationLength);
      modifiedFenestrationWidth = parseInt(modifiedFenestrationWidth);
    } else {
      hasBothSizes = false;
    }

    if (hasLength === true && hasWidth === false) {
      hasBothSizes = false;
    } else if (hasWidth === true && hasLength === false) {
      hasBothSizes = false;
    }

    if ((modifiedFenestrationLength == "" || modifiedFenestrationLength == undefined || modifiedFenestrationLength == null) && (modifiedFenestrationWidth == "" || modifiedFenestrationWidth == undefined || modifiedFenestrationWidth == null)) {
      hasSizeInput = false;
    } else {
      hasSizeInput = true;
    }
  }

  function checkLocation() {
    if (modifiedFenestrationDistance) {
      hasLocation = true;
      modifiedFenestrationDistance = parseInt(modifiedFenestrationDistance);
    } else {
      hasLocation = false;
      modifiedFenestrationDistance = 0;
    }

    if (modifiedFenestrationDistance == "" || modifiedFenestrationDistance == undefined || modifiedFenestrationDistance == null || modifiedFenestrationDistance == 0) {
      hasLocationInput = false;
    } else {
      hasLocationInput = true;
    }
  }

  function validateFenestration() {
    if (myTubeFenestration && myTubeFenestration == "modified") {
      // check for size and location
      checkSize();
      checkLocation();
      if (hasLocation === true || hasBothSizes === true) {
        $('#fenestrationFieldset .js-go-forward').click();
      } else {
        errorBarText.innerHTML = "You must enter values for size or location to advance with a modified fenestration."
        triggerErrorBar();
      }
    } else if (myTubeFenestration) {
      $('#fenestrationFieldset .js-go-forward').click();
    } else {
      errorBarText.innerHTML = "Please choose your fenestration to advance."
      triggerErrorBar();
    }
  }

  $('.js-save-fenestration-size').on('click', function() {
    checkSize();
    if (hasBothSizes === true && hasSizeInput === true) {
      var _this = $(this);
      closeAccordion(_this);
      modifiedFenestrationLength = parseInt(modifiedFenestrationLength);
      modifiedFenestrationWidth = parseInt(modifiedFenestrationWidth);
    }
    else if (hasBothSizes === false) {
      errorBarText.innerHTML = "Please enter a length and width to modify your fenestration size."
      triggerErrorBar();
    }
  });

  $('.js-save-fenestration-distance').on('click', function() {
    checkLocation();
    if (hasLocation && hasLocationInput) {
      var _this = $(this);
      closeAccordion(_this);
      modifiedFenestrationDistance = parseInt(modifiedFenestrationDistance);
    }
    else {
      errorBarText.innerHTML = "Please enter your distance to advance."
      triggerErrorBar();
    }
  });

  function closeAccordion(_this) {
    var thisContent = _this.parent('.js-accordion-content');
    var thisIcon = _this.parent().siblings().children('.js-accordion-icon');
    var thisTrigger = _this.parent().siblings('.js-accordion-trigger');
    // Close the answer item - it's already open
    thisContent.removeClass('is-active').slideUp(300);
    // change the icon to its default state
    thisIcon.removeClass('mi-btb-minus').addClass('mi-btb-plus').removeClass('is-active');
  }

  $('.js-validate-fenestration').on('click', function() {
    validateFenestration();
  });
  //************ FENESTRATION VALIDATION ************//


  //************ TUBE QUANTITY ************//
  let tubeQuantity = document.getElementById('tubeQuantity');
  let myTubeQuantity = tubeQuantity.value;
  $('.js-show-tube-quantity').html(myTubeQuantity);

  tubeQuantity.addEventListener("change", function() {
    myTubeQuantity = tubeQuantity.value;
    $('.js-show-tube-quantity').html(myTubeQuantity);
  });
  //************ TUBE QUANTITY ************//


  //************ FINALIZE ORDER REQUEST  ************//
  $('#finalizeRequest').on('click', function() {
    finalizeValues();
    displayValues();
  });

  function finalizeValues() {
    myProductCode = (myProductCode).toUpperCase();
    myTubeSize = myTubeSize;
    myTubeLength = capitalizeFirstLetter(myTubeLength);
    myTubeCuff = capitalizeFirstLetter(myTubeCuff);
    myTubeFlange = capitalizeFirstLetter(myTubeFlange);
    myTubeCurve = capitalizeFirstLetter(myTubeCurve);
    myTubeFenestration = capitalizeFirstLetter(myTubeFenestration);
  }

  function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
  }

  function displayValues() {
    showMyTubeCode();
    showMyTubeSize();
    showMyTubeLength();
    showMyTubeCuff();
    showMyTubeFlange();
    showMyTubeCurve();
    showMyTubeFenestration();
  }

  function showMyTubeCode() {
    $('.js-show-tube-code').html(myProductCode);
  }

  function showMyTubeSize() {
    $('.js-show-tube-size').html(myTubeSize);
  }

  function showMyTubeLength() {
    $('.js-show-tube-length').html(myTubeLength);

    if (myTubeLength === "Standard") {
      $('.js-if-modified-length').hide();
      $('.js-if-standard-length').show();
      $('.js-show-tube-default-length').html(myTotalLength);
    } else {
      $('.js-if-standard-length').hide();
      $('.js-if-modified-length').show();
      $('.js-show-modified-proximal-length').html(newProximal);
      $('.js-show-modified-distal-length').html(newDistal);
      $('.js-show-total-modified-length').html(totalModifiedLength);
    }
  }

  function showMyTubeCuff() {
    $('.js-show-tube-cuff').html(myTubeCuff);
  }

  function showMyTubeCurve() {
    $('.js-show-tube-curve').html(myTubeCurve);

    if (myTubeCurve === "Modified") {
      $('.js-if-modified-curve').show();
      $('.js-show-modified-curve-angle').html(modifiedCurveAngle);
    } else {
      $('.js-if-modified-curve').hide();
    }
  }

  function showMyTubeFlange() {
    $('.js-show-tube-flange').html(myTubeFlange);

    if (modifiedFlangeRotation) {
      $('.js-if-rotated-flange').show();
      $('.js-show-modified-flange-rotation').html(modifiedFlangeRotation);
      $('.js-show-modified-flange-direction').html(flangeDirection);
    } else {
      $('.js-if-rotated-flange').hide();
    }
  }

  function showMyTubeFenestration() {
    $('.js-show-tube-fenestration').html(myTubeFenestration);

    if (hasLocation === true) {
      $('.js-if-modified-fenestration-location').show();
      $('.js-show-modified-fenestration-distance').html(modifiedFenestrationDistance);
    } else {
      $('.js-if-modified-fenestration-location').hide();
    }

    if (hasBothSizes === true) {
      $('.js-if-modified-fenestration-size').show();
      $('.js-show-modified-fenestration-length').html(modifiedFenestrationLength);
      $('.js-show-modified-fenestration-width').html(modifiedFenestrationWidth);
    } else {
      $('.js-if-modified-fenestration-size').hide();
    }
  }
  //************ FINALIZE ORDER REQUEST ************//
});
