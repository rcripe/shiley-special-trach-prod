function validateReorderForm() {
  return REORDER.validateForm();
}

;REORDER = {

  validateForm: function() {
    // input fields
    let purchaseOrderNum         = $("#purchaseOrderNum");
    let shipToAccountNum         = $("#shipToAccountNum");
    let prevLotNum               = $("#prevLotNum");
    let prevPurchaseOrderNum     = $("#prevPurchaseOrderNum");
    let product                  = $('#product');
    let tubeQuantity             = $('#tubeQuantity');
    let facility                 = $("#facility");
    let address                  = $("#address");
    let city                     = $("#city");
    let state                    = $("#state");
    let zip                      = $("#zip");
    let country                  = $("#country");
    let email                    = $("#email");
    let billToAccountNum         = $("#billToAccountNum");
    let billingFacility          = $("#billingFacility");
    let billingAddress           = $("#billingAddress");
    let billingCity              = $("#billingCity");
    let billingState             = $("#billingState");
    let billingZip               = $("#billingZip");
    let billingCountry           = $("#billingCountry");
    let billingEmail             = $("#billingEmail");

    let fields = [];
    let approvedInput = [];
    let isValid,
        zipRegex,
        diffBillingInfo,
        billZipRegex;

    let hasDifferentBillingInfo = $('input[name="billingInformation"]:checked').val();

    if (hasDifferentBillingInfo === "differentBilling") {
      diffBillingInfo = true;
      fields.push(purchaseOrderNum, shipToAccountNum, prevLotNum, prevPurchaseOrderNum, product, tubeQuantity, facility, address, city, state, zip, country, email, billToAccountNum, billingFacility, billingAddress, billingCity, billingState, billingZip, billingCountry, billingEmail);
    } else {
      fields.push(purchaseOrderNum, shipToAccountNum, prevLotNum, prevPurchaseOrderNum, product, tubeQuantity, facility, address, city, state, zip, country, email);
    }

    // test for any input 2 chars or more
    let generalRegex = /^(([a-zA-Z ]|[0-9 ])|([-]|[_]|[.])){1,40}$/;
    let stringRegex = /^([a-zA-Z -]){1,40}$/;
    let numRegex = /^[0-9]{1,5}$/;
    // international email test
    let emailRegex = /^(([a-zA-Z]|[0-9])|([-]|[_]|[.]))+[@](([a-zA-Z0-9])|([-])){2,63}[.](([a-zA-Z0-9]){2,63})+$/;
    // united states zip code
    let usaZipRegex = /^\d{5}([ \-]\d{4})?$/;
    // canadian zip code
    let canadaZipRegex = /^[ABCEGHJKLMNPRSTVXY]\d[ABCEGHJ-NPRSTV-Z][ ]?\d[ABCEGHJ-NPRSTV-Z]\d$/;
    // israeli zip code
    let israelZipRegex = /^\\b\\d{5}(\\d{2})?$/;

    let myCountry = $('#country').val();
    let myBillingCountry = $('#billingCountry').val();

    let myState = $('#state').val();
    let myBillingState = $('#billingState').val();

    // based on country selection, add state/province to required fields
    // add appropriate zip regex
    if (myCountry === "USA") {
      zipRegex = usaZipRegex;
    } else if (myCountry === "Canada") {
      zipRegex = canadaZipRegex;
    } else if (myCountry === "Israel") {
      zipRegex = israelZipRegex;
      myState = "NA";
    } else {
      zipRegex = /^\d{5}([ \-]\d{4})?$/;
    }

    // do the same for billing fields if applicable
    if (myBillingCountry === "USA") {
      billZipRegex = usaZipRegex;
    } else if (myBillingCountry === "Canada") {
      billZipRegex = canadaZipRegex;
    } else if (myBillingCountry === "Israel") {
      billZipRegex = israelZipRegex;
      myBillingState = "NA";
    } else {
      billZipRegex = /^\d{5}([ \-]\d{4})?$/;
    }

    function validateInput(element, index, array) {
      let $element = element;
      let value = $element.val();
      let myLength = value.length;
      let myTest;

      // add different tests based on the field requirements
      if ($element.hasClass("js-email")) {
        myTest = emailRegex.test(value);
      } else if ($element.hasClass("js-zip")) {
        myTest = zipRegex.test(value);
      } else if ($element.hasClass("js-billing-zip")) {
        myTest = billZipRegex.test(value);
      } else if ($element.hasClass('is-num')) {
        myTest = numRegex.test(value);
      }
        else {
        myTest = generalRegex.test(value);
      }

      if (myTest == false || myLength < 1) {
        $element.addClass('has-focus');
        // show the error message if input doesn't pass
        $element.next().show();
        isValid = false;
      } else if (myTest === true && myLength >= 1) {
        $element.removeClass('has-focus');
        // hide the error message if input passes the test
        $element.next().hide();
        // isValid = true;
        approvedInput.push(value);
      }
    }

    fields.forEach(validateInput);

    if (approvedInput.length === fields.length) {
      isValid = true;
    } else {
      isValid = false;
    }

    return isValid;
  }
}
