$(document).ready(function() {

  var $overlay = document.getElementById('overlay');

  $('.js-trigger-modal').click(function(e) {
    $('#modal').fadeIn(200);
    var isOverlayShown = $overlay.classList.contains('show-overlay');
    $overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  });

  $('.js-close-modal').click(function() {
      $('#modal').fadeOut(200);
      var isOverlayShown = $overlay.classList.contains('show-overlay');
      $overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  });
});
