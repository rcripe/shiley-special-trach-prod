$(document).ready(function() {

  $('.fieldset:first-of-type').show();

  $('#orderNav').hide();
  $('#order').hide();

  // click function for 'next' button
  $('.js-go-forward').on('click', function($this) {
    let current_fs = $(this).parent();
    // sets next_fs to next fieldset
    let next_fs = $(this).parent().next();
    // hides current fieldset
    current_fs.hide();
    // shows next fieldset
    next_fs.fadeIn(300);
    // find matching next fs icon
    let next_icon = $(next_fs).data('attr');
    let current_icon = $(current_fs).data('attr');
    // declare nav icon container
    let icon_nav = $('.js-nav-items');
    // add active state to next icon
    $(icon_nav).find('.js-nav-icon[data-attr="' + current_icon + '"]').addClass('is-validated');
    $(icon_nav).find('.js-nav-icon[data-attr="' + next_icon + '"]').addClass('is-active');

    if ($(next_fs).data('attr') === 'finalize') {
      $('.js-nav-icon').addClass('is-active').addClass('is-validated');
      $('.faux-breadcrumbs').show();
      $('#disclaimer').show();
    } else {
      $('.faux-breadcrumbs').hide();
      $('#disclaimer').hide();
    }
  });

  $('.js-go-to-order').on('click', function() {
    $('#customize').hide();
    $('#customizeNav').hide();
    $('#disclaimer').hide();
    $('.js-fieldset').hide();
    $('#order').show();
    $('#orderNav').show();
    $('#orderDetails').fadeIn(300);
    $('.js-nav-items').find('.js-nav-item[data-attr="orderDetails"]').addClass('is-active');
    $('.faux-breadcrumbs').show();
  });

  $('.js-go-to-customer-info').on('click', function() {
    $('.js-fieldset').hide();
    $('#disclaimer').hide();
    $('#customerInfo').fadeIn(300);
    $('.js-nav-items').find('.js-nav-item[data-attr="customerInfo"]').addClass('is-active');
    $('.js-nav-items').find('.js-nav-item[data-attr="orderDetails"]').addClass('is-validated');
  });

  // click function for 'back' button
  $('.js-go-back').on('click', function() {
    // sets current_fs to current fieldset
    let current_fs = $(this).parent().parent().parent().parent();
    // sets prev_fs to prev fieldset
    let prev_fs = $(this).parent().parent().parent().parent().prev();
    // hides current fieldset
    current_fs.hide();
    // shows prev fieldset
    prev_fs.fadeIn(300);
    // find matching prev fs icon
    let prev_icon = $(prev_fs).data('attr');
    let current_icon = $(current_fs).data('attr');
    // declare nav icon container
    let icon_nav = $('.js-nav-items');
    // remove active state from icons
    $(icon_nav).find('.js-nav-icon[data-attr="' + current_icon + '"]').removeClass('is-active');
    // add active state to prev icon
    $(icon_nav).find('.js-nav-icon[data-attr="' + prev_icon + '"]').addClass('is-active').removeClass('is-validated');
  });


  // start over - start form again
  $('.js-restart-form').on('click', function() {
    $('.faux-breadcrumbs').hide();
    $('#disclaimer').hide();
    $('.fieldset').hide();
    $('.fieldset:first-of-type').fadeIn(300);
    $('.js-nav-icon').removeClass('is-active').removeClass('is-validated');
    $('.js-nav-icon:first-of-type').addClass('is-active');
    document.getElementById('mainForm').reset();
  });

  $('.js-back-to-tube-selections').on('click', function() {
    $('.faux-breadcrumbs').hide();
    $('#disclaimer').hide();
    $('#order').hide();
    $('#orderNav').hide();
    $('#customizeNav').show();
    $('#orderNav .js-nav-item').removeClass('is-active').removeClass('is-validated');
    $('#customize').fadeIn(300);
    $('#fenestrationFieldset').fadeIn(300);
    $('#fenestrationIcon').removeClass('is-validated');
  });

});
