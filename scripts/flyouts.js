$(document).ready(function() {
  var $formSlider = document.getElementById('formSlider');
  var $helpSlider = document.getElementById('helpSlider');
  var $overlay = document.getElementById('overlay');
  var $toggleForm = $('.js-form-toggle');
  var $toggleHelp = $('.js-help-toggle');
  var $toggleBoth = $('.js-toggle-both');

  $toggleForm.on('click', function() {
    toggleForm();
  });

  $toggleHelp.on('click', function() {
    toggleHelp();
  });

  $toggleBoth.on('click', function() {
    toggleHelp();
    toggleForm();
  });

  function toggleForm() {
    var isOpen = $formSlider.classList.contains('form-slide-in');
    var isOverlayShown = $overlay.classList.contains('show-overlay');
    $formSlider.setAttribute('class', isOpen ? 'form-slide-out' : 'form-slide-in');
    $overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }

  function toggleHelp() {
    var isOpen = $helpSlider.classList.contains('help-slide-in');
    var isOverlayShown = $overlay.classList.contains('show-overlay');
    $helpSlider.setAttribute('class', isOpen ? 'help-slide-out' : 'help-slide-in');
    $overlay.setAttribute('class', isOverlayShown ? 'hide-overlay' : 'show-overlay');
  }
});
