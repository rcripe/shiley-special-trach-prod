$(document).ready(function() {

  // Initiialize variables
  var allIcons = $('.js-accordion-icon').addClass('mi-btb-plus');
  var question = $('.js-accordion-question');

  // Detect click on question container
  $(question).click(function(e) {
    // Set variables in a clicked item
    $thisQuestion = $(this);
    $thisAnswer = $(this).next('.js-accordion-answer');
    $thisIcon = $(this).children('.js-accordion-icon');
    // if the answer is already open
    if($thisAnswer.hasClass('is-active')) {
      // Close the answer item
      $thisAnswer.removeClass('is-active').slideUp(300);
      // change the color of the question to its default state
      $thisQuestion.css('color', '#53565A');
      // change the icon to its default state
      $thisIcon.removeClass('mi-btb-minus').addClass('mi-btb-plus').removeClass('is-active');
    } else {
      // open this answer
      $thisAnswer.addClass('is-active').slideDown(300);
      // change the color of this question
      $thisQuestion.css('color', '#0085CA');
      // change the color and icon for this question item
      $thisIcon.removeClass('mi-btb-plus').addClass('mi-btb-minus').addClass('is-active');
    }
  });

  // Initiialize variables
  var allIcons = $('.js-accordion-icon').addClass('mi-btb-plus');
  var trigger = $('.js-accordion-trigger');

  // Detect click on trigger container
  $(trigger).click(function(e) {
    // Set variables in a clicked item
    $thisTrigger = $(this);
    $thisContent = $(this).next('.js-accordion-content');
    $thisIcon = $(this).children('.js-accordion-icon');
    // if the answer is already open
    if($thisContent.hasClass('is-active')) {
      // Close the answer item
      $thisContent.removeClass('is-active').slideUp(300);
      // change the icon to its default state
      $thisIcon.removeClass('mi-btb-minus').addClass('mi-btb-plus').removeClass('is-active');
    } else {
      // open this answer
      $thisContent.addClass('is-active').slideDown(300);
      // change the color and icon for this trigger item
      $thisIcon.removeClass('mi-btb-plus').addClass('mi-btb-minus').addClass('is-active');
    }
  });

  $('.js-close-accordion-button').on('click', function() {
    $thisContent = $(this).parent('.js-accordion-content');
    $thisIcon = $(this).parent().siblings().children('.js-accordion-icon');
    $thisTrigger = $(this).parent().siblings('.js-accordion-trigger');
    // Close the answer item - it's already open
    $thisContent.removeClass('is-active').slideUp(300);
    // change the icon to its default state
    $thisIcon.removeClass('mi-btb-minus').addClass('mi-btb-plus').removeClass('is-active');
  });

  // radio button to accordion
  var radioSelector = $('.js-radio-selector');
  var radioContent = $('.js-radio-content');

  $(radioSelector).click(function(e) {
    $(radioContent).toggleClass('is-active').slideToggle(300);
  });

  // radio button to accordion
  var radioSlideUp = $('.js-radio-slide-up');
  var radioSlideDown = $('.js-radio-slide-down');
  var radioSlider = $('.js-radio-slider');

  $(radioSlideUp).click(function(e) {
    $(radioSlider).addClass('is-active').slideUp(300);
  });

  $(radioSlideDown).click(function(e) {
    $(radioSlider).addClass('is-active').slideDown(300);
  });


});
